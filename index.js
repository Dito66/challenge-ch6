const express = require("express");
const app = express();
const user = require("./api/user");

app.set("view engine", "ejs");

app.use("/api/v1", user);

// list view
app.get("/", (req, res) => {
  res.render("users/list");
});

// form create
app.get("/users/form", (req, res) => {
  res.render("users/form", { id: null });
});

// form edit
app.get("/users/form/:id", (req, res) => {
  res.render("users/form", {
    id: req.params.id,
  });
});

app.listen(8000, () => console.log(`Server running on port 8000`));
