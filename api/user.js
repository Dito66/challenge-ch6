const express = require("express");
const router = express.Router();
const cors = require("cors");
const { User } = require("../models");

router.use(cors());
router.use(express.json());

// list all users
router.get("/users", async (req, res) => {
  let response = [];
  try {
    response = await User.findAll();
  } catch (e) {
    console.log(e.message);
  }

  res.json(response);
});

// get single user by id
router.get("/users/:id", async (req, res) => {
  let response = {};
  try {
    response = await User.findOne({ where: { id: req.params.id } });
  } catch (e) {
    console.log(e.message);
    response.error = e.message;
    res.status(401);
  }

  res.json(response);
});

// create new user
router.post("/users", async (req, res) => {
  try {
    await User.create({
      password: req.body.password,
      name: req.body.name,
      approved: req.body.approved === true,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  } catch (e) {
    console.log(e.message);
  }
  res.end();
});

// update user by id
router.put("/users/:id", async (req, res) => {
  try {
    await User.update(
      {
        password: req.body.password,
        name: req.body.name,
        approved: req.body.approved === true,
      },
      {
        where: { id: req.params.id },
      }
    );
  } catch (e) {
    console.log(e.message);
  }

  res.end();
});

// remove user by id
router.delete("/users/:id", async (req, res) => {
  try {
    await User.destroy({
      where: { id: req.params.id },
    });
  } catch (e) {
    console.log(e.message);
  }

  res.end();
});

module.exports = router;
